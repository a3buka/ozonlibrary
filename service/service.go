package service

import "gitlab.com/a3buka/ozonlibrary/internal/domain/entity"

type serviceAuthor interface {
	Create(name string) error
	FindAll() (u []entity.Author, err error)
	Generate() error
}

type serviceUser interface {
	Create(firstName, lastName string) error
	FindAll() (u []entity.User, err error)
	UserTakeBook(idBook, idUser string) error
	GiveBook(idBook, idUser string) error
	Generate() error
}

type serviceBook interface {
	Create(FakeBook entity.BookAdd) error
	FindAll() ([]entity.Book, error)
	Generate() error
}

type Service struct {
	Author serviceAuthor
	Book   serviceBook
	User   serviceUser
}

func NewService(repAuthor serviceAuthor, repBook serviceBook, repUser serviceUser) *Service {
	return &Service{Author: repAuthor, Book: repBook, User: repUser}
}
