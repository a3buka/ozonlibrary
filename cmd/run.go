package run

import (
	"context"
	"errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/a3buka/ozonlibrary/handler"
	"gitlab.com/a3buka/ozonlibrary/internal/author/db"
	db2 "gitlab.com/a3buka/ozonlibrary/internal/book/db"

	"gitlab.com/a3buka/ozonlibrary/internal/server"
	db3 "gitlab.com/a3buka/ozonlibrary/internal/user/db"
	"gitlab.com/a3buka/ozonlibrary/pkg/client"
	"gitlab.com/a3buka/ozonlibrary/service"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func Run() {
	posgresSQLclient := client.NewClient()
	authorRep := db.NewAuthorRepository(posgresSQLclient)
	bookRep := db2.NewRepositoryBook(posgresSQLclient)
	userRep := db3.NewRepositoryUser(posgresSQLclient)

	ServiceLibrary := service.NewService(authorRep, bookRep, userRep)
	handlerLibrary := handler.NewHandler(ServiceLibrary)
	serverLibrary := server.NewServer(handlerLibrary.InitRoutes())

	go func() {
		if err := serverLibrary.Run(); !errors.Is(err, http.ErrServerClosed) {
			logrus.Errorf("server it's not worked %s", err.Error())
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit

	logrus.Print("TodoApp Shutting Down")

	if err := serverLibrary.Shutdown(context.Background()); err != nil {
		logrus.Errorf("error occured on server shutting down: %s", err.Error())
	}

}
