package book

import "gitlab.com/a3buka/ozonlibrary/internal/domain/entity"

type RepositoryBook interface {
	Create(FakeBook entity.BookAdd) error
	FindAll() ([]entity.Book, error)
	Close()
	Generate() error
}
