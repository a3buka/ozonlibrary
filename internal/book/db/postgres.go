package db

import (
	"errors"
	"github.com/jmoiron/sqlx"
	"gitlab.com/a3buka/ozonlibrary/internal/book"
	"gitlab.com/a3buka/ozonlibrary/internal/domain/entity"
	"gitlab.com/a3buka/ozonlibrary/pkg/generator"
)

type repository struct {
	db *sqlx.DB
}

func (r *repository) Generate() error {
	var count int
	query := "SELECT COUNT(*) FROM public.book"
	err := r.db.Get(&count, query)
	if err != nil {
		return err
	}
	if count < 100 {
		bookGen := generator.GenerationBook()
		for j := range bookGen {
			err = r.Create(bookGen[j])
			if err != nil {
				return err
			}
		}
	}
	//defer r.db.Close()
	return err
}

func (r *repository) Create(FakeBook entity.BookAdd) error {

	//var exists bool
	////var book entity.Book
	//
	//queryFind := "SELECT EXISTS (SELECT id FROM public.author WHERE id = $1)"
	//_ = r.db.Get(&exists, queryFind, FakeBook.AuthorId) // Если такого ид нет то фалсе иначе тру
	//if exists == false {
	//	return errors.New("author with id not found")
	//}
	var existsCheck bool
	queryCheck := "SELECT EXISTS (SELECT title, author_id FROM public.book WHERE title=$1 AND author_id=$2)"
	_ = r.db.Get(&existsCheck, queryCheck, FakeBook.Title, FakeBook.AuthorId)
	if existsCheck == true {
		return errors.New("author and title with create yet")
	}
	queryAdd := "INSERT INTO public.book (title, author_id, book_taken) VALUES ($1, $2, $3) RETURNING id, title, author_id, busy, book_taken"

	_, err := r.db.Exec(queryAdd, FakeBook.Title, FakeBook.AuthorId, 0)
	if err != nil {
		return err
	}

	//Генерация

	return nil
}

func (r *repository) FindAll() ([]entity.Book, error) {
	var book entity.Book
	var books []entity.Book
	query := "SELECT * FROM public.book"
	row, err := r.db.Query(query)
	defer row.Close()
	if err != nil {
		return nil, err
	}
	for row.Next() {
		err = row.Scan(&book.Id, &book.Title, &book.AuthorId, &book.Busy, &book.TakenBy)
		if err != nil {
			return nil, err
		}
		books = append(books, book)
	}
	return books, nil
}

func NewRepositoryBook(client *sqlx.DB) book.RepositoryBook {
	return &repository{db: client}
}

func (r *repository) Close() {
	r.db.Close()
}
