package db

import (
	"errors"
	"github.com/jmoiron/sqlx"
	"gitlab.com/a3buka/ozonlibrary/internal/domain/entity"
	"gitlab.com/a3buka/ozonlibrary/internal/user"
	"gitlab.com/a3buka/ozonlibrary/pkg/generator"
)

type repository struct {
	db *sqlx.DB
}

func (r *repository) GiveBook(idBook, idUser string) error {
	var existsUser bool //Проверка на наличие юзера
	queryBusyUser := "SELECT EXISTS (SELECT id FROM public.user WHERE id = $1)"
	_ = r.db.Get(&existsUser, queryBusyUser, idUser)
	if existsUser == false {
		return errors.New("the user is not found")
	}
	var book entity.Book
	queryGiveBook := "SELECT busy, book_taken FROM public.book WHERE id = $1"
	err := r.db.QueryRow(queryGiveBook, idBook).Scan(&book.Busy, &book.TakenBy)
	if err != nil {
		return err
	}
	if book.Busy != true && book.TakenBy != idUser {
		return errors.New("book with this user not found")
	}
	queryUpdateStatusBook := "UPDATE public.book SET busy = $1, book_taken = $2 WHERE id = $3"
	err = r.db.QueryRow(queryUpdateStatusBook, false, 0, idBook).Err()
	if err != nil {
		return err
	}
	return nil
}

func (r *repository) UserTakeBook(idBook, idUser string) error {

	var existsUser bool //Проверка на наличие юзера
	queryBusyUser := "SELECT EXISTS (SELECT id FROM public.user WHERE id = $1)"
	_ = r.db.Get(&existsUser, queryBusyUser, idUser)
	if existsUser == false {
		return errors.New("the user is not found")
	}

	var book entity.Book
	err := r.db.QueryRow("SELECT busy FROM public.book WHERE id = $1", idBook).Scan(&book.Busy)
	if err != nil {
		return err
	}
	if book.Busy == true {
		return errors.New("the book is busy yet")
	}
	queryUpdate := "UPDATE public.book SET busy = $1, book_taken = $2 WHERE id = $3 "
	_, err = r.db.Exec(queryUpdate, true, idUser, idBook)
	if err != nil {
		return err
	}

	return nil
}

func (r *repository) Generate() error {
	//генерация
	var count int
	query := "SELECT COUNT(*) FROM public.user"
	err := r.db.Get(&count, query)
	if err != nil {
		return err
	}
	if count < 50 {
		userGen := generator.GenerationUser()
		for j := range userGen {
			err = r.Create(userGen[j].FirstName, userGen[j].LastName)
			if err != nil {
				return err
			}
		}
	}
	//defer r.db.Close()
	return nil
}

func (r *repository) Create(firstName, lastName string) error {

	dbf := `INSERT INTO public.user (firstName, lastName) VALUES ($1, $2) RETURNING id`
	_, err := r.db.Exec(dbf, firstName, lastName)
	if err != nil {
		return err
	}
	//defer r.db.Close()
	return nil
}

func (r *repository) FindAll() (u []entity.User, err error) {
	var user entity.User
	var users []entity.User
	query := "SELECT * FROM public.user"
	row, err := r.db.Query(query)
	defer row.Close()
	if err != nil {
		return nil, err
	}
	for row.Next() {
		err = row.Scan(&user.Id, &user.FirstName, &user.LastName)
		if err != nil {
			return nil, err
		}
		//работа с книгами
		var book entity.Book
		queryBook := "SELECT * FROM public.book"
		row1, err := r.db.Query(queryBook)
		defer row1.Close()
		if err != nil {
			return nil, err
		}
		//Проходим по книгам
		for row1.Next() {
			err = row1.Scan(&book.Id, &book.Title, &book.AuthorId, &book.Busy, &book.TakenBy)
			if err != nil {
				return nil, err
			}
			if book.TakenBy == user.Id {
				user.RentedBook = append(user.RentedBook, book)
			}
		}
		users = append(users, user)
		user.RentedBook = nil
	}
	return users, nil
}

func NewRepositoryUser(client *sqlx.DB) user.RepositoryUser {
	return &repository{db: client}
}

func (r *repository) Close() {
	r.db.Close()
}
