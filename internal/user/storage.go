package user

import "gitlab.com/a3buka/ozonlibrary/internal/domain/entity"

type RepositoryUser interface {
	Create(firstName, lastName string) error
	FindAll() (u []entity.User, err error)
	UserTakeBook(idBook, idUser string) error
	GiveBook(idBook, idUser string) error
	Generate() error
	Close()
}
