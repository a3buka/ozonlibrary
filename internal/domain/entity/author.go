package entity

type Author struct {
	ID    string `db:"id" json:"id"`
	Name  string `db:"name" json:"name"`
	Books []Book `db:"books" json:"books"`
}

type AuthorDTO struct {
	Name string `db:"name" json:"name"`
}
