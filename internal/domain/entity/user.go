package entity

type User struct {
	Id         string `db:"id" json:"id"`
	FirstName  string `db:"firstName" json:"firstName"`
	LastName   string `db:"lastName" json:"lastName"`
	RentedBook []Book `db:"rentedBook" json:"rentedBook"`
}
