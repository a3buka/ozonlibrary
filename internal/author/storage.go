package author

import "gitlab.com/a3buka/ozonlibrary/internal/domain/entity"

type RepositoryAuthor interface {
	Create(name string) error
	FindAll() (u []entity.Author, err error)
	Generate() error
}
