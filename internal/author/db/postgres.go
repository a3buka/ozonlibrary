package db

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/a3buka/ozonlibrary/internal/author"
	"gitlab.com/a3buka/ozonlibrary/internal/domain/entity"
	"gitlab.com/a3buka/ozonlibrary/pkg/generator"
)

type repository struct {
	db *sqlx.DB
}

func (r *repository) Generate() error {
	//Генерация
	var count int
	query := "SELECT COUNT(*) FROM public.author"
	err := r.db.Get(&count, query)
	if err != nil {
		return err
	}

	if count < 10 {
		authorGen := generator.GenerationAuthor()
		for j := range authorGen {
			err = r.Create(authorGen[j].Name)
			if err != nil {
				return err
			}
		}
	}
	//defer r.db.Close()
	return nil
}

func NewAuthorRepository(client *sqlx.DB) author.RepositoryAuthor {
	return &repository{db: client}
}

func (r *repository) Close() {
	r.db.Close()
}

func (r *repository) Create(author string) error {

	dbf := `INSERT INTO public.author (name) VALUES ($1) RETURNING id`
	_, err := r.db.Exec(dbf, author)
	if err != nil {
		return err
	}

	return nil

}

func (r *repository) FindAll() ([]entity.Author, error) {
	var author entity.Author
	var authors []entity.Author
	query := "SELECT id,name FROM public.author"
	row, err := r.db.Query(query)
	if err != nil {
		return nil, err
	}
	//Проходим по авторам
	for row.Next() {
		err = row.Scan(&author.ID, &author.Name)
		if err != nil {
			return nil, err
		}
		var book entity.Book
		queryBook := "SELECT * FROM public.book"
		row1, err := r.db.Query(queryBook)
		if err != nil {
			return nil, err
		}
		//Проходим по книгам
		for row1.Next() {
			err = row1.Scan(&book.Id, &book.Title, &book.AuthorId, &book.Busy, &book.TakenBy)
			if err != nil {
				return nil, err
			}
			if author.ID == book.AuthorId {
				author.Books = append(author.Books, book)
			}
		}

		authors = append(authors, author)
		author.Books = nil
	}

	return authors, nil
}

//func (r *repository) TopAuthors()
