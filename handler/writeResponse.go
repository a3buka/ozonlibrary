package handler

import (
	"encoding/json"
	"net/http"
)

func writeResponse(w http.ResponseWriter, data interface{}) {
	// выставляем заголовки, что отправляем json в utf8
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err := json.NewEncoder(w).Encode(data)

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
