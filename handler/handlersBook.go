package handler

import (
	"encoding/json"
	"gitlab.com/a3buka/ozonlibrary/internal/domain/entity"
	"net/http"
)

func (h *Handler) AddBook(w http.ResponseWriter, r *http.Request) {
	var bookAdd entity.BookAdd
	err := json.NewDecoder(r.Body).Decode(&bookAdd)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	err = h.service.Book.Create(bookAdd)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

}

func (h *Handler) WatchAllBook(w http.ResponseWriter, r *http.Request) {
	Books, err := h.service.Book.FindAll()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	writeResponse(w, Books)
}

func (h *Handler) GenerateBook(w http.ResponseWriter, r *http.Request) {
	err := h.service.Book.Generate()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}
