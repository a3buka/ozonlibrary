package handler

import (
	"net/http"
)

func (h *Handler) AddUser(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil { // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	firstName := r.FormValue("firstName")
	if firstName == "" {
		http.Error(w, "Empty name", http.StatusBadRequest)
		return
	}
	lastName := r.FormValue("lastName")
	if lastName == "" {
		http.Error(w, "Empty last name", http.StatusBadRequest)
		return
	}

	err = h.service.User.Create(firstName, lastName)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}

func (h *Handler) WatchAllUser(w http.ResponseWriter, r *http.Request) {
	Users, err := h.service.User.FindAll()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	writeResponse(w, Users)
}

func (h *Handler) UserTake(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil { // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	IdBook := r.FormValue("idBook")
	if IdBook == "" {
		http.Error(w, "Empty id book", http.StatusBadRequest)
		return
	}
	IdUser := r.FormValue("idUser")
	if IdUser == "" {
		http.Error(w, "Empty id user", http.StatusBadRequest)
		return
	}
	err = h.service.User.UserTakeBook(IdBook, IdUser)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}

func (h *Handler) UserGive(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil { // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	IdBook := r.FormValue("idBook")
	if IdBook == "" {
		http.Error(w, "Empty id book", http.StatusBadRequest)
		return
	}
	IdUser := r.FormValue("idUser")
	if IdUser == "" {
		http.Error(w, "Empty id user", http.StatusBadRequest)
		return
	}
	err = h.service.User.GiveBook(IdBook, IdUser)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}

func (h *Handler) GenerateUser(w http.ResponseWriter, r *http.Request) {
	err := h.service.User.Generate()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}
