package handler

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/a3buka/ozonlibrary/service"
	"net/http"
)

type Handler struct {
	service *service.Service
}

func NewHandler(service *service.Service) *Handler {
	return &Handler{service: service}
}

func (h *Handler) InitRoutes() *chi.Mux {
	router := chi.NewRouter()
	router.Use(middleware.Logger)

	//Author
	router.Post("/author/add", h.AddAuthor)
	router.Get("/author/watchAll", h.WatchAllAuthor)
	router.Get("/author/generate", h.GenerateAuthor)
	//User
	router.Post("/user/add", h.AddUser)
	router.Get("/user/watchAll", h.WatchAllUser)
	router.Post("/user/rentBook", h.UserTake)
	router.Post("/user/giveBook", h.UserGive)
	router.Get("/user/generate", h.GenerateUser)
	//Book
	router.Post("/book/add", h.AddBook)
	router.Get("/book/watchAll", h.WatchAllBook)
	router.Get("/book/generate", h.GenerateBook)

	router.Get("/swagger", SwaggerUI)

	router.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./public"))).ServeHTTP(w, r)
	})

	return router
}
