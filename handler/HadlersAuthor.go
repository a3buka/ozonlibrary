package handler

import (
	"net/http"
)

func (h *Handler) GenerateAuthor(w http.ResponseWriter, r *http.Request) {
	err := h.service.Author.Generate()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}

func (h *Handler) AddAuthor(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil { // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	AuthorName := r.FormValue("author")
	if AuthorName == "" {
		http.Error(w, "Empty author name", http.StatusBadRequest)
		return

	}
	err = h.service.Author.Create(AuthorName)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}

func (h *Handler) WatchAllAuthor(w http.ResponseWriter, r *http.Request) {
	Authors, err := h.service.Author.FindAll()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	writeResponse(w, Authors)
}
