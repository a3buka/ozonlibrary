package client

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	_ "github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"log"
	"os"
)

const createDB = `
CREATE SCHEMA IF NOT EXISTS public;

CREATE TABLE IF NOT EXISTS public.user(
    id SERIAL PRIMARY KEY ,
    firstName VARCHAR NOT NULL,
    lastName VARCHAR NOT NULL

);

CREATE TABLE IF NOT EXISTS public.author(
    id SERIAL PRIMARY KEY ,
    name VARCHAR NOT NULL
);

CREATE TABLE IF NOT EXISTS public.book(
    id SERIAL PRIMARY KEY ,
    title VARCHAR NOT NULL,
    author_id INTEGER NOT NULL,
    FOREIGN KEY (author_id) REFERENCES public.author (id),
    busy BOOLEAN DEFAULT FALSE,
    book_taken INTEGER DEFAULT 0,
    FOREIGN KEY (book_taken) REFERENCES public.user (id)
);

INSERT INTO public.user (id, firstName, lastName)
SELECT 0, 'library', 'library'
WHERE NOT EXISTS (
    SELECT 1 FROM public.user WHERE id = 0
);
`

func NewClient() (db *sqlx.DB) {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	db, err = sqlx.Open(os.Getenv("DB_DRIVER"), fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=%s",
		os.Getenv("DB_HOST"), os.Getenv("DB_PORT"), os.Getenv("DB_USER"),
		os.Getenv("DB_NAME"), os.Getenv("DB_PASSWORD"), "disable"))
	if err != nil {
		log.Fatal(err)
	}
	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}
	_, err = db.Exec(createDB)
	if err != nil {
		log.Fatal(err)
	}

	return
}
