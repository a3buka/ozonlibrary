package generator

import (
	"github.com/brianvoe/gofakeit/v6"
	"gitlab.com/a3buka/ozonlibrary/internal/domain/entity"
	"strconv"
)

func GenerationUser() []entity.User {
	gofakeit.Seed(0)
	users := make([]entity.User, 60)

	for i := 0; i < 60; i++ {
		user := entity.User{
			Id:        strconv.Itoa(i),
			FirstName: gofakeit.FirstName(),
			LastName:  gofakeit.LastName(),
		}
		users[i] = user
	}
	return users
}

func GenerationAuthor() []entity.Author {
	gofakeit.Seed(0)
	authors := make([]entity.Author, 15)

	for i := 0; i < 15; i++ {
		author := entity.Author{
			ID:   strconv.Itoa(i),
			Name: gofakeit.AppAuthor(),
		}
		authors[i] = author
	}
	return authors
}

func GenerationBook() []entity.BookAdd {
	gofakeit.Seed(0)
	books := make([]entity.BookAdd, 120)
	for i := 0; i < 120; i++ {
		book := entity.BookAdd{
			Title:    gofakeit.Sentence(2),
			AuthorId: strconv.Itoa(gofakeit.IntRange(1, 10)),
		}
		books[i] = book
	}
	return books
}
