module gitlab.com/a3buka/ozonlibrary

go 1.19

require (
	github.com/go-chi/chi/v5 v5.0.8
	github.com/jmoiron/sqlx v1.3.5
	github.com/lib/pq v1.10.9
	github.com/sirupsen/logrus v1.9.2
)

require (
	github.com/brianvoe/gofakeit/v6 v6.21.0 // indirect
	github.com/joho/godotenv v1.5.1 // indirect
	golang.org/x/sys v0.8.0 // indirect
)
