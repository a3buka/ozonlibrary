package docs

import "gitlab.com/a3buka/ozonlibrary/internal/domain/entity"

//go:generate swagger generate spec -o ./public/swagger.json --scan-models

// swagger:route POST /book/add book bookAddRequest
// Добавление новой книги.
// responses:
// 	200: description: successfully
// 	400: description: BadRequest

// swagger:parameters bookAddRequest
type bookAddRequest struct {
	//required:true
	//in:body
	Book entity.BookAdd
}

// swagger:route GET /book/watchAll book bookWatchAllRequest
// Список всех книг.
// responses:
//	 200: bookWatchAllResponse
//	 500: description: Internal server Error

// swagger:parameters bookWatchAllRequest

// swagger:response bookWatchAllResponse
type bookWatchAllResponse struct {
	//in:body
	Book []entity.Book
}

// swagger:route GET /book/generate book bookGenerateRequest
// Сгенерировать 120 книг.
// responses:
//	 200: bookGenerateResponse
// swagger:parameters bookGenerateRequest

// swagger:response bookGenerateResponse
type bookGenerateResponse struct {
	//in:body
	Book []entity.Book
}
