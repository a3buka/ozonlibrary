package docs

import "gitlab.com/a3buka/ozonlibrary/internal/domain/entity"

//go:generate swagger generate spec -o ./public/swagger.json --scan-models

//  swagger:route POST /author/add author authorCreateRequest
// Добавление нового автора.
// responses:
// 	200: description: successfully
// 	400: description: BadRequest

// swagger:parameters authorCreateRequest
type authorCreateRequest struct {
	//required:true
	//in:formData
	Author string `json:"author"`
}

// swagger:route GET /author/watchAll author authorWatchAllRequest
// Список всех авторов.
// responses:
//	 200: authorWatchAllResponse
//	 500: description: Internal server Error

// swagger:parameters authorWatchAllRequest

// swagger:response authorWatchAllResponse
type authorWatchAllResponse struct {
	//in:body
	Author []entity.Author
}

// swagger:route GET /author/generate author authorGenerateRequest
// Сгенерировать 10 авторов.
// responses:
//	 200: authorWatchAllResponse
// swagger:parameters authorGenerateRequest

// swagger:response authorGenerateResponse
type authorGenerateResponse struct {
	//in:body
	Author []entity.Author
}
