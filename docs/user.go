package docs

import "gitlab.com/a3buka/ozonlibrary/internal/domain/entity"

//go:generate swagger generate spec -o ./public/swagger.json --scan-models

//  swagger:route POST /user/add user userCreateRequest
// Добавление нового пользователя.
// responses:
// 	200: description: successfully
// 	400: description: BadRequest

// swagger:parameters userCreateRequest
type userCreateRequest struct {
	//required:true
	//in:formData
	FirstName string `json:"firstName"`
	//required:true
	//in:formData
	LastName string `json:"lastName"`
}

// swagger:route GET /user/watchAll user userWatchAllRequest
// Список всех пользователей.
// responses:
//	 200: userWatchAllResponse
//	 500: description: Internal server Error

// swagger:parameters userWatchAllRequest

// swagger:response  userWatchAllResponse
type userWatchAllResponse struct {
	//in:body
	User []entity.User
}

// swagger:route POST /user/rentBook user userRentBookRequest
// Взять книгу из библиотеки.
// responses:
// 	200: description: successfully
//	400: description: Bad request

// swagger:parameters userRentBookRequest
type userRentBookRequest struct {
	//required:true
	//in:formData
	IdBook string `json:"idBook"`
	//required:true
	//in:formData
	IdUser string `json:"idUser"`
}

// swagger:route POST /user/giveBook user userGiveBookRequest
// Сдать книгу в библиотеку.
// responses:
// 	200: description: successfully
//	400: description: Bad request

// swagger:parameters userGiveBookRequest
type userGiveBookRequest struct {
	//required:true
	//in:formData
	IdBook string `json:"idBook"`
	//required:true
	//in:formData
	IdUser string `json:"idUser"`
}

// swagger:route GET /user/generate user userGenerateRequest
// Сгенерировать 50 пользователей.
// responses:
//	 200: userGenerateResponse
// swagger:parameters userGenerateRequest

// swagger:response userGenerateResponse
type userGenerateResponse struct {
	//in:body
	Book []entity.Book
}
